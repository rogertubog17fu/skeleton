[![pipeline status](https://gitlab.com/tdd_class/skeleton/badges/master/pipeline.svg)](https://gitlab.com/tdd_class/skeleton/commits/master)

# Test Driven Development class
Respository for the Test Driven Development workshop at the [Nightowl Hackerspace](http://nightowl.foundationu.com) of [Foundation University](http://www.foundationu.com)

This repository contains the skeleton code to get you started.
See also the pdf in the documentation subdirectory.

## Installation on Windows
  - Download python 3 from https://www.python.org/downloads/
  - Install it, check the options to install pip and add python to the Path
  - Download and install git from https://git-scm.com/downloads
  - In the commandline run the followig:

```
pip install virtualenv virtualenvwrapper-win
git clone https://gitlab.com/tdd_class/skeleton.git
cd skeleton
mkvirualenv env
workon env
pip install -r requirements.txt

```

## Installation on Linux
For all Debian based distros (Ubuntu, Mint, etc) run

```
apt-get install python3 virtualenvwrapper git pip3
git clone https://gitlab.com/tdd_class/skeleton.git
cd skeleton
mkvirualenv env
workon env
pip install -r requirements.txt
```

For all other distributions only the first line is different where apt-get and potentially the package names might be different for your distro. Red-Hat (Fedora) based distros can use yum or dnf instead of apt-get.

## Running
In the commandline run the following from the skeleton directory:

```
workon env
python run.py
```
The application then runs on http://127.0.0.1:5000

## Unit tests
This is what the course is all about. To run the tests, from the skeleton directory
run the following:

```
workon env
pytest
```

Without any changes to the code, this will result in failed unittests. This is intentional.
Part of the assignments is to improve the code.

This will search in the tests folder for all unittests and runs them one by one.
