import math


def add(a, b):
	try:
		a = float(a)	
		b = float(b)
		return a+b
	except ValueError:
		return "cannot convert letter to number"	


def sqrt(num): 
	try: 
		num = float(num)
		if num<0:
			result = "cannot square negative integer"
		else:
			result = math.sqrt(num)
	except ValueError:
		result = 'cannot convert letter to number'
	return result  	

def divide(a, b):	
	try:
		a = float(a)
		b = float(b)
		if b == 0.0:
			result = "cannot divide %d by zero"%a
		else:
			result = a/b
	except ValueError:
		result = "cannot convert letter to number"
	return result
